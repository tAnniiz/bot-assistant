from enum import Enum

class Base(Enum):
    def has_value(cls, value):
        return (any(value == item.value for item in cls))