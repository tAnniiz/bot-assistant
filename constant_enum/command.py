from constant_enum.base import Base
from models.app_configuration import App_Configuration

class Command(Base):
    SHUTDOWN = 0
    RESTART = 1
    GET_STAT = 2
    CAP_SC = 3
    LOGIN_FACEBOOK_MESSENGER = 4

    @staticmethod
    def get_valid_commands():
        default_cmds = [ 'SHUTDOWN', 'RESTART', 'GET_STAT', 'CAP_SC', 'LOGIN_FACEBOOK_MESSENGER' ]
        custom_get_gpu = App_Configuration.shortcuts_get_status
        custom_capture_ss = App_Configuration.shortcuts_capture_screen
        custom_login_facebook_messenger = App_Configuration.shortcuts_login_facebook_messenger

        return default_cmds + custom_get_gpu + custom_capture_ss + custom_login_facebook_messenger
