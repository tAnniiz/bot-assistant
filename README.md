# bot-assistant
- Implemented based on Python 3.5.4

# Prerequsites
- Python <= 3.5.4
- pip is required
- pyinstaller is required for windows executable build

# Get Started
- execute "pip install -r dev_requirements.txt"
- first run as dev with "python app.py"

# To build the project to executable(.exe) for Windows
- execute "pyinstaller app.py --onefile --hidden-import=queue" in cmd
- copy original "config.json" to executing directory
