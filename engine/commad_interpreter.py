from constant_enum.command import Command
from models.command_info import CommandInfo

class Command_Interpreter:
    fn_mapping = {}

    @staticmethod
    def register_fn(enum_name, fn):
        if not hasattr(Command_Interpreter.fn_mapping, enum_name):
            Command_Interpreter.fn_mapping[enum_name] = fn

    @staticmethod
    def get_fn(enum_name):
        return Command_Interpreter.fn_mapping[enum_name]

    @staticmethod
    def compose_cmd_info(str_cmd):
        splitted_str = str_cmd.split(' ')

        cmd = splitted_str[0]
        args = [arg for arg in splitted_str[1:]]

        return CommandInfo(cmd, args)