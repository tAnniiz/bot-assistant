from fbchat import Client
from fbchat.models import *
from constant_enum.command import Command
from engine.commad_interpreter import Command_Interpreter
from subprocess import Popen
from sys import stderr
from time import sleep
from models.gpu_info import GPU_Info
from models.app_configuration import App_Configuration
from imgurpython import ImgurClient
from engine.authentication import Authentication
import tempfile
import os
import wx
import sys

class Assistant(Client):
    ## GLOBAL VARIABLE
    # APP_FEATURE_CONFIG
    _authentication_feature = False
    #_authen = None
   
    def __init__(self, email, pwd):
        Client.__init__(self, email, pwd)
        #self._authentication_feature = App_Configuration.authentication_feature
        #self._authen = Authentication()
        self.register_fn()

    def register_fn(self):
        Command_Interpreter.register_fn(Command.GET_STAT.name, self.onGetGPUStat)
        for keyword in App_Configuration.shortcuts_get_status:
            Command_Interpreter.register_fn(keyword, self.onGetGPUStat)

        Command_Interpreter.register_fn(Command.CAP_SC.name, self.onCaptureScreen)
        for keyword in App_Configuration.shortcuts_capture_screen:
            Command_Interpreter.register_fn(keyword, self.onCaptureScreen)
        
        # if self._authentication_feature:
        #     Command_Interpreter.register_fn(Command.LOGIN_FACEBOOK_MESSENGER.name, self._authen.onLoginFacebookMessenger)
        #     for keyword in App_Configuration.shortcuts_login_facebook_messenger:
        #         Command_Interpreter.register_fn(keyword, self._authen.onLoginFacebookMessenger)

    def onGetGPUStat(self, args):
        if self.is_self_command(args) == False:
            return None

        temp_filename = tempfile.NamedTemporaryFile(mode='w')
        temp_filename.close()
        nvidia_tmp_filename = temp_filename.name

        local_nvidia_smi = '\"C:/Program Files/NVIDIA Corporation/NVSMI/nvidia-smi.exe\"'
        bundled_nvidia_smi = '\"./thirdparty/nvidia-smi/nvidia-smi.exe\"'

        nvidia_smi_path = local_nvidia_smi if os.path.isfile('C:/Program Files/NVIDIA Corporation/NVSMI/nvidia-smi.exe') else bundled_nvidia_smi

        query_gpu_command = nvidia_smi_path + ' --query-gpu=index,name,temperature.gpu,power.draw,utilization.gpu --format=csv -f {}'.format(nvidia_tmp_filename)
        p = Popen(query_gpu_command, stdout=stderr, stderr=open(os.devnull, 'w'), shell=True)
        p.wait()
        assert not p.returncode, 'ERROR: Call to stanford_ner exited with a non-zero code status.'
        output = []
        sleep(1)

        with open(nvidia_tmp_filename) as f:
            content = f.readlines()

        lines = [x.strip() for x in content[1:]]

        gpu_infoes = []
        for line in lines:
            splitted = line.replace(' ', '').split(',')
            gpu_infoes.append(GPU_Info(splitted[0], splitted[1], splitted[2], splitted[3], splitted[4]))

        message = []
        if App_Configuration.worker_name != None:
            message.append('Worker: {0}'.format(App_Configuration.worker_name))

        for gpu_info in gpu_infoes:
            message.append(gpu_info.to_string())

        return "\n".join(message)

    def onCaptureScreen(self, args):
        if self.is_self_command(args) == False:
            return None
        
        temp_filename = tempfile.NamedTemporaryFile(mode='w')
        temp_filename.close()
        tmp_filename = temp_filename.name
        save_path = tmp_filename + '.png'

        app = wx.App()
        screen = wx.ScreenDC()
        size = screen.GetSize()
        bmp = wx.Bitmap(size[0], size[1])
        mem = wx.MemoryDC(bmp)
        mem.Blit(0, 0, size[0], size[1], screen, 0, 0)
        del mem 
        bmp.SaveFile(save_path, wx.BITMAP_TYPE_PNG)

        client_id = App_Configuration.imgur_client
        client_secret =  App_Configuration.imgur_secret

        client_imgur = ImgurClient(client_id, client_secret)
        uploaded_image = client_imgur.upload_from_path(path=save_path)

        return uploaded_image['link']

    def onMessage(self, author_id, message, thread_id, thread_type, **kwargs):
        self.markAsDelivered(author_id, thread_id)
        self.markAsRead(author_id)

        print('Incomming message: ' + message)

        if author_id != self.uid:
            cmd_info = Command_Interpreter.compose_cmd_info(message)

            msg_temp = message.split('.') 
            if len(msg_temp) >= 2: 
                message = msg_temp[1] 
                username = msg_temp[2]         

            if cmd_info.command in Command.get_valid_commands(): 
                fn = Command_Interpreter.get_fn(cmd_info.command) 

                if self._authentication_feature: 
                    if self._authen._USER.getStatus(): 
                        messages = fn(cmd_info.args) 
                else: 
                    messages = fn(cmd_info.args) 

                if messages != None:
                    if isinstance(messages, list):
                        for msg in messages:
                            self.sendMessage(msg, thread_id=thread_id, thread_type=thread_type)
                    else:
                        image_url = messages + '.png'
                        if fn.__name__ == 'onCaptureScreen':
                            self.sendRemoteImage(image_url = image_url, thread_id=thread_id, thread_type=thread_type)
                        else:
                            self.sendMessage(messages, thread_id=thread_id, thread_type=thread_type)

    def is_self_command(self, args):
        if len(args) < 1:
            return True

        if App_Configuration.worker_name != None and App_Configuration.worker_name not in args:
            return False

        return True