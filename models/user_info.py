class USER_Info:
    user_id = None
    status = False

    def __init__(self):
        user_id = ''

    def setUserId(self, user_id):
        self.user_id = user_id

    def getUserId(self):
        if self.user_id is None:
            return ''
        return self.user_id

    def setStatus(self, status):
        self.status = status

    def getStatus(self):
        return self.status

    def isLogin(self):
        if self.status:
            return True
        return False

    def to_string(self):
        return 'user_id => ' + self.getUserId() + ' :: status => ' + str(self.status)