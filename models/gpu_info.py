class GPU_Info:
    index = None
    name = None
    temperature = None
    power = None
    utilization = None

    def __init__(self, index, name, temperature, power, utilization):
        self.index = index
        self.name = name
        self.temperature = temperature
        self.power = power
        self.utilization = utilization if 'Error' not in utilization else 'n/a'

    def to_string(self):
        return ('GPU{0} {1}: usage {2} temp {3}c power {4}').format(self.index, self.name, self.utilization, self.temperature, self.power)