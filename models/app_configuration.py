import json

class App_Configuration:
    facebook_email = None
    facebook_pwd = None
    imgur_client = None
    imgur_secret = None
    worker_name = None
    authentication_feature = None
    auto_reconnect = True
    reconnect_max_tries = None
    reconnect_interval = 10
    shortcuts_get_status = []
    shortcuts_capture_screen = []
    shortcuts_login_facebook_messenger = []

    @staticmethod
    def load_app_configuration():
        with open('config.json') as data_file:    
            data = json.load(data_file)

        App_Configuration.facebook_email = data['facebook_email']
        App_Configuration.facebook_pwd = data['facebook_password']
        App_Configuration.imgur_client = data['imgur_client_id']
        App_Configuration.imgur_secret = data['imgur_client_secret']
        App_Configuration.worker_name = None  if data['worker_name'] == '' else data['worker_name']
        App_Configuration.auto_reconnect = data['auto_reconnect']
        App_Configuration.reconnect_max_tries = None if data['reconnect_max_tries'] == 0 else data['reconnect_max_tries']
        App_Configuration.reconnect_interval = data['reconnect_interval']

        App_Configuration.shortcuts_get_status = data['shortcuts_get_status']
        App_Configuration.shortcuts_capture_screen = data['shortcuts_capture_screen']
        # App_Configuration.shortcuts_login_facebook_messenger = data['shortcuts_login_facebook_messenger']

        #APP FEATURE
        # App_Configuration.authentication_feature = data["authentication_feature"]