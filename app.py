from engine.assistant import Assistant
from models.app_configuration import App_Configuration
from time import sleep
import queue as Queue

def start_facebook_session(max_tries):
    if max_tries != None and max_tries == 0:
        return

    try:
        client = Assistant(App_Configuration.facebook_email, App_Configuration.facebook_pwd)
        client.listen()
    except:
        print("Connection is lost, trying to reconnect in {0} seconds".format(App_Configuration.reconnect_interval))
        sleep(App_Configuration.reconnect_interval)
        start_facebook_session(None if max_tries == None else max_tries - 1)

App_Configuration.load_app_configuration()
start_facebook_session(1 if App_Configuration.auto_reconnect == 0 else App_Configuration.reconnect_max_tries)
